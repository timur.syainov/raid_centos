#!/bin/bash
# hardware space(GB)


#***************Homework************************
#В дополнении посчитать вывод Size Avail в K G +
#Реализовать вывод ошибки в случае некорректного ввода. Чему равняется $1, если он в case не KMG
#Вывести значение в процентом соотношени(%10+1).Подсчитать общую сумму. +

df -h

#*************************** colors ********************************
Yellow='\e[93m'
Reset='\e[39m'
Magnet='\e[35m'
Gren='\e[32m'
Red='\e[31m'

#***************************** variable declaration *****************
#declare -i Sum_Avail=0
#declare -j Sum_Size=0
#declare -g Sum_Use=0
 

#*************************** main **************************
 
Avail_space_array=$(df | grep ^/dev/ | tr -s ' ' | cut -d ' '  -f 4)
Size_space_array=$(df | grep ^/dev/ | tr -s ' ' | cut -d ' '  -f 2)
Use_space_array=$(df | grep ^/dev/ | tr -s ' ' | cut -d ' ' -f 5 | tr % ' ') 


#******************* Avaid ***************************************

array=($Avail_space_array)
for i in ${array[@]};
  do
    let "Sum_Avail+=i"
  done


#***************** Output values Avail in KB******************
Avail_Kb=$(echo "scale=3; $Sum_Avail" | bc) 
echo -e  "$Red Avail_space: $Avail_Kb"Kb" $Reset"

#***************** Output values Avail in MB******************
Avail_Mb=$(echo "scale=3; $Sum_Avail/1024" | bc)
echo -e  "$Yellow Avail_space: $Avail_Mb"Mb" $Reset"

#***************** Output values Avail in GB******************
Avail_Gb=$(echo "scale=3; $Sum_Avail/1024/1024" | bc)
echo -e  "$Gren Avail_space: $Avail_Gb"Gb" $Reset"



#****************************** Size ****************************

array=($Size_space_array)
for j in ${array[@]};
  do
    let "Sum_Size+=j"
  done

#function in_range() {


 # echo "checking if $ is in (]"
  #if [[ $(echo "" |bc) == 1  && $(echo "" |bc) == "1" ]]; then
   # echo "between"
  #else
   # echo "not between"
  #fi
#}

#in_range


#***************** Output values Size in KB******************
Size_Kb=$(echo "scale=3; $Sum_Size" | bc)
echo -e  "$Red Size_space: $Size_Kb"Kb" $Reset"

#***************** Output values Size in MB******************
Size_Mb=$(echo "scale=3; $Sum_Size/1024" | bc)
echo -e  "$Yellow Size_space: $Size_Mb"Mb" $Reset"

#***************** Output values Size in GB******************
Size_Gb=$(echo "scale=3; $Sum_Size/1024/1024" | bc)
echo -e  "$Gren Size_space: $Size_Gb"Gb" $Reset"


#****************************** Use %****************************



array=($Use_space_array)
for g in ${array[@]};
 do
  let "Sum_Use+=g"
  done

#****************************** Output values Use in SUMM % ****************************



Use=$(echo "scale=3; $Sum_Use" | bc)
echo -e  "$Magnet Use_space_ALL: $Use"%" $Reset" 


